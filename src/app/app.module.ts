import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { GetfilmComponent } from './getfilm/getfilm.component';
import { GetallfilmsComponent } from './getallfilms/getallfilms.component';
import { NewfilmComponent } from './newfilm/newfilm.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UpdatefilmComponent } from './updatefilm/updatefilm.component';
import { BadroutingComponent } from './badrouting/badrouting.component';

@NgModule({
  declarations: [
    AppComponent,
    GetfilmComponent,
    GetallfilmsComponent,
    NewfilmComponent,
    UpdatefilmComponent,
    BadroutingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: GetallfilmsComponent },
      { path: 'films/new', component: NewfilmComponent },
      { path: 'films/:id/update', component: UpdatefilmComponent },
      { path: 'films', component: GetallfilmsComponent },
      { path: 'films/:id', component: GetfilmComponent },
      { path: '**', component: BadroutingComponent }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
