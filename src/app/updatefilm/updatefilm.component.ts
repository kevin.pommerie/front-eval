import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Film } from '../films';

@Component({
  selector: 'app-updatefilm',
  templateUrl: './updatefilm.component.html',
  styleUrls: ['./updatefilm.component.css']
})
export class UpdatefilmComponent implements OnInit {

  film: Observable<Film>;

  idFromRoute = 0;

  checkoutForm = this.formBuilder.group({
    title: ['', Validators.pattern('[a-zA-Z \-\']*')],
    director: ['', Validators.pattern('[a-zA-Z \-\']*')],
    releaseYear: ['', Validators.pattern('[0-9]{4}')],
    imdbRating: ['', Validators.pattern('(^[0-9](.[0-9])?$)|10')],
    synopsis: ''
  });

  constructor(private httpClient: HttpClient, private formBuilder: FormBuilder, private route: ActivatedRoute) {
    const routeParams = this.route.snapshot.paramMap;
    this.idFromRoute = Number(routeParams.get("id"));
    this.film = this.httpClient.get<Film>(environment.apiUrl + 'films/' + this.idFromRoute);
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.httpClient.put<Film>(environment.apiUrl + 'films/' + this.idFromRoute, this.checkoutForm.value).subscribe();
    window.alert("film modifié");
    this.checkoutForm.reset();
    location.replace('http://localhost:4200/films/'+this.idFromRoute);
  }

}
