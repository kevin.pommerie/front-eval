import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Film } from '../films';
import { FormBuilder, FormControl, FormGroup, FormsModule, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-newfilm',
  templateUrl: './newfilm.component.html',
  styleUrls: ['./newfilm.component.css']
})
export class NewfilmComponent implements OnInit {

  checkoutForm = this.formBuilder.group({
    title: ['', Validators.pattern('[a-zA-Z \-\']*')],
    director: ['', Validators.pattern('[a-zA-Z \-\']*')],
    releaseYear: ['', Validators.pattern('[0-9]{4}')],
    imdbRating: ['', Validators.pattern('(^[0-9](.[0-9])?$)|10')],
    synopsis: ''
  });

  constructor(private httpClient: HttpClient, private formBuilder: FormBuilder) {

  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.httpClient.post<Film>(environment.apiUrl + 'films', this.checkoutForm.value).subscribe();
    window.alert("film ajouté");
    this.checkoutForm.reset();
    location.replace('http://localhost:4200/films/');
  }

}
