import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

import { Film } from "./films";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'front-eval';

  films: Observable<Film[]>;
  
  constructor(private httpClient: HttpClient) { 
    this.films = httpClient.get<Film[]>(environment.apiUrl+"films");
  }

}
