import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetallfilmsComponent } from './getallfilms.component';

describe('GetallfilmsComponent', () => {
  let component: GetallfilmsComponent;
  let fixture: ComponentFixture<GetallfilmsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetallfilmsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetallfilmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
