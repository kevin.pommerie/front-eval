import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

import { Film } from "../films";

@Component({
  selector: 'app-getallfilms',
  templateUrl: './getallfilms.component.html',
  styleUrls: ['./getallfilms.component.css']
})
export class GetallfilmsComponent implements OnInit {

  films: Observable<Film[]>;

  constructor(private httpClient: HttpClient) { 
    this.films = httpClient.get<Film[]>(environment.apiUrl+"films");
  }

  ngOnInit(): void {
  }

  onClick(id: number) {
    this.httpClient.delete<Film>(environment.apiUrl+'films/' + id).subscribe();
    window.alert("film supprimé");
    location.replace('http://localhost:4200/films/');
  }

}
