import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetfilmComponent } from './getfilm.component';

describe('GetfilmComponent', () => {
  let component: GetfilmComponent;
  let fixture: ComponentFixture<GetfilmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetfilmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetfilmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
