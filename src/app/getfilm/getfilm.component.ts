import { Component, OnInit } from '@angular/core';

import { Film } from '../films';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-getfilm',
  templateUrl: './getfilm.component.html',
  styleUrls: ['./getfilm.component.css']
})
export class GetfilmComponent implements OnInit {

  film: Observable<Film>;

  constructor(private httpClient: HttpClient, private route: ActivatedRoute) { 
    const routeParams = this.route.snapshot.paramMap;
    const idFromRoute = Number(routeParams.get("id"));
    this.film = httpClient.get<Film>(environment.apiUrl+'films/' + idFromRoute);
  }

  ngOnInit(): void {
  }

  
  onClick(id: number) {
    this.httpClient.delete<Film>(environment.apiUrl+'films/' + id).subscribe();
    window.alert("film supprimé");
    location.replace('http://localhost:4200/films/');
  }
  
}
