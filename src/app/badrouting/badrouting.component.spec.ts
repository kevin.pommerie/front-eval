import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BadroutingComponent } from './badrouting.component';

describe('BadroutingComponent', () => {
  let component: BadroutingComponent;
  let fixture: ComponentFixture<BadroutingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BadroutingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BadroutingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
